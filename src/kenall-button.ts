import { LitElement, html } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { KENALL } from '@ken-all/kenall';

@customElement('kenall-button')
export class KenAllButton extends LitElement {
    @property()
    target?: string = 'data-kenall';

    @property()
    apiKey?: string = '';

    private async onClick(e: Event) {
        if (!this.apiKey) {
            console.warn(`<kenall-button>: apiKey attribute is empty`);
            return;
        }
        const postalCode = document.querySelector<HTMLInputElement>(
            `*[${this.target}="postal-code"]`
        );
        const prefecture = document.querySelector<HTMLInputElement>(
            `*[${this.target}="prefecture"]`
        );
        const city = document.querySelector<HTMLInputElement>(
            `*[${this.target}="city"]`
        );
        const address = document.querySelector<HTMLInputElement>(
            `*[${this.target}="address"]`
        );
        if (postalCode && postalCode.value) {
            console.log(postalCode.value, this.apiKey);
            const ka = new KENALL(this.apiKey);
            const r = await ka.getAddress(postalCode.value);
            console.log(r.data);
            r.data[0];
            if (r.data.length > 0) {
                const d = r.data[0];
                if (prefecture) {
                    prefecture.value = d.prefecture;
                }
                if (city) {
                    city.value = d.city;
                }
                if (address) {
                    if (!prefecture && !city) {
                        address.value = `${d.prefecture}${d.city}${d.town}`;
                    } else {
                        address.value = d.town;
                    }
                }
            }
        } else {
            const address: Array<string> = [];
        }
    }

    render() {
        return html` <button @click="${this.onClick}">〠住所検索</button> `;
    }
}
